<?php

namespace App\Controller;

use App\Helper\SomeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/main', name: 'app_main')]
    public function index(): Response
    {
        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }
    #[Route('/', name: 'app_main_home',methods: ['GET'])]
    public function home(): Response{
        //return new Response("<h1>Accueil</h1>");
        return $this->render("main/home.html.twig");
    }

    #[Route('/test', name: 'app_main_test',methods: ['GET'])]
    public function test(): Response{
        //return new Response("<h1>Test</h1>");
        $serie = [
            "title" => "Game of Thrones",
            "year" => 2000
        ];

        $serieHTML = [
            "title" => "<h1>Game of Thrones</h1>",
            "year" => 2000
        ];
        return $this->render("main/test.html.twig",[
            "mySerie" => $serie,
            "mySerieHTML" => $serieHTML,
            "autreVar" => 412412
        ]);
    }

    #[Route('/contact', name: 'app_main_contactus',methods: ['GET'])]
    public function contactUs(): Response{
        return $this->render("main/contact_us.html.twig");
    }
    #[Route('/show', name: 'app_main_show',methods: ['GET'])]
    public function show(SomeService $someService): Response {
        $someService->doSomething();
        return $this->render("main/contact_us.html.twig");
    }
}
