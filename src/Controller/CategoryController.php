<?php

namespace App\Controller;

use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
#[Route('/categories')]
class CategoryController extends AbstractController
{
    #[Route('/{id}/supprimer', name: 'category_delete', methods: ['GET'])]
    public function delete(Category $category, Request $request,
                           EntityManagerInterface $em) : Response
    {
        try {
            if (count($category->getCourses()) > 0 ) {
                // si la catégorie possède des cours, on les supprime
                foreach ($category->getCourses() as $course) {
                    $category->removeCourse($course);
                }
            }
            $em->remove($category);
            $em->flush();
            $this->addFlash('success', 'La catégorie a été supprimée');
        } catch (\Exception $ex) {
            $this->addFlash('danger', 'La catégorie n\'a pas pu être supprimée<br>'.$ex->getMessage());
        }
        return $this->redirectToRoute('course_list');
    }
}
