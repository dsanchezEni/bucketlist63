<?php

namespace App\Controller;

use App\Entity\Course;
use App\Form\CourseType;
use App\Repository\CourseRepository;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/cours')]
class CourseController extends AbstractController
{
//    #[Route('/course', name: 'app_course')]
//    public function index(): Response
//    {
//        return $this->render('course/index.html.twig', [
//            'controller_name' => 'CourseController',
//        ]);
//    }

    #[Route('', name: 'course_list', methods: ['GET'])]
    public function list(CourseRepository $courseRepository): Response
    {
        //$courses = $courseRepository->findAll();
        //$courses = $courseRepository->findBy([], ['name' => 'DESC'], 5);
        $courses = $courseRepository->findLastCourses();
        return $this->render('course/list.html.twig', [
            'courses' => $courses
        ]);
    }

    #[Route('/{id}', name: 'course_show', requirements: ['id' => '\d+'], methods:['GET'])]
    public function show(Course $course, CourseRepository $courseRepository): Response
    {
        //$course = $courseRepository->find($id);
        //if (!$course) {
        // throw $this->createNotFoundException('Cours inconnu');
        //}
        return $this->render('course/show.html.twig', [
            'course' => $course
        ]);
    }

    #[Route('/ajouter', name: 'course_create', methods: ['GET', 'POST'])]
    public function create(Request $request,EntityManagerInterface $em, FileUploader $fileUploader): Response
    {
        //dd($request);
        //dump($request);

        $course = new Course();
        $courseForm = $this->createForm(CourseType::class, $course);
        dump($course);
        // traiter le formulaire
        $courseForm->handleRequest($request);
        dump($course);

        if ($courseForm->isSubmitted() && $courseForm->isValid()) {
            // traitement de l'image
            $imageFile = $courseForm->get('image')->getData();
            if ($imageFile) {
                $course->setFilename($fileUploader->upload($imageFile));
            }
            $em->persist($course);
            $em->flush();

            $this->addFlash('success', 'Le cours a été ajouté');

            return $this->redirectToRoute('course_show', ['id' => $course->getId()]);
        }

        return $this->render('course/create.html.twig', [
            'courseForm' => $courseForm
        ]);
    }

    #[Route('/{id}/modifier', name: 'course_edit', requirements: ['id' => '\d+'], methods: ['GET', 'POST'])]
    public function edit(Course $course, Request $request, EntityManagerInterface $em,FileUploader $fileUploader): Response
    {
        // TODO : rechercher le cours dans la bdd d'après son ID
        $courseForm = $this->createForm(CourseType::class, $course);
        $courseForm->handleRequest($request);
        if ($courseForm->isSubmitted() && $courseForm->isValid()) {
            // hydrate les propriétés absentes du formulaire
            $course->setDateModified(new \DateTimeImmutable());
            // traitement de l'image

            $imageFile = $courseForm->get('image')->getData();
            if (($courseForm->has('deleteImage') && $courseForm['deleteImage']->getData()) || $imageFile) {
                // suppression de l'ancienne image
                // si on a coché l'option dans le formulaire
                // ou si on a changé d'image
                $fileUploader->delete($course->getFilename(), $this->getParameter('app.images_course_directory'));
                if ($imageFile) {
                    $course->setFilename($fileUploader->upload($imageFile));
                }
                else {
                    $course->setFilename(null);
                }
            }
            // le persist() n'est pas nécessaire, car Doctrine connait déjà l'objet
            $em->flush();
            $this->addFlash('success', 'Le cours a été modifié');
            return $this->redirectToRoute('course_show', ['id' => $course->getId()]);
        }
        return $this->render('course/edit.html.twig',[
            'courseForm' => $courseForm
        ]);
    }

    #[Route('/demo', name : 'course_demo', methods: ['GET'])]
    public function demo(EntityManagerInterface $em,ValidatorInterface $validator): Response
    {
        // créer une instance de l'entité
        $course = new Course();
        // hydrater toutes les propriétés
        $course->setName('Symfony');
        $course->setContent('Le développement web côté serveur (avec Symfony)');
        $course->setDuration(10);
        $course->setDateCreated(new \DateTimeImmutable());
        $course->setPublished(true);
        $errors = $validator->validate($course);
        if (count($errors) == 0) {
            $em->persist($course);
            $em->flush();
            return $this->redirectToRoute('course_show', ['id' => $course->getId()]);
        }
        dd($errors);
//        $em->persist($course);
//        dump($course);
//
//        $em->persist($course);
//        // ne pas oublier le flush() sinon l'objet ne sera pas persisté en base.
//        $em->flush();
//        dump($course);
//        // on modifie l'objet
//        $course->setName('PHP');
//        // on sauvegarde l'objet
//        $em->flush();
//        dump($course);
//        $em->remove($course);
//        $em->flush();
//        return $this->render('course/create.html.twig');
    }

    #[Route('/{id}/supprimer', name: 'course_delete', requirements: ['id' => '\d+'], methods: ['GET'])]
    public function delete(Course $course, Request $request, EntityManagerInterface $em): Response
    {
        if ($this->isCsrfTokenValid('delete-'.$course->getId(), $request->get('token'))) {
            try {
                $em->remove($course);
                $em->flush();
                $this->addFlash('success', 'Le cours a été supprimé');
            } catch (\Exception $ex) {
                $this->addFlash('danger', 'Le cours n\' pas pu être supprimé');
            }
        } else {
            $this->addFlash('danger', 'Le cours n\' pas pu être supprimé : pb token');
        }
        return $this->redirectToRoute('course_list');
    }
}

