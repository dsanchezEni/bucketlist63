<?php

namespace App\Controller;

use App\Models\Region;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ApiController extends AbstractController
{
    #[Route('/regions',name:'app_regions',methods:['GET'])]
    public function regions(SerializerInterface $serializer):Response{
        //On consomme l'API de la liste des régions du site "geo.api.gouv.fr"
        $regions=file_get_contents('https://geo.api.gouv.fr/regions');
       /* $regionsTab=$serializer->decode($regions,'json');
        $regionsObjet=$serializer->denormalize($regionsTab,Region::class.'[]');*/

        $lesRegions = $serializer->deserialize($regions,Region::class.'[]','json');
        //dd($regionsObjet);
        //dd($regions);
        //dd($regionsTab);
        return $this->render('api/regions.html.twig',['regions'=>$lesRegions]);
    }
}