<?php

namespace App\Controller;

use App\Form\SearchEventType;
use App\Models\SearchEvent;
use App\Service\EventService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EventController extends AbstractController
{
    #[Route('/events', name: 'app_events',methods: ['GET','POST'])]
    public function events(Request $request, EventService $eventService): Response
    {
        $searchEvent = new SearchEvent();
        $searchEvent->dateEvent = new \DateTimeImmutable();
        $eventForm = $this->createForm(SearchEventType::class, $searchEvent);
        $eventForm->handleRequest($request);
        if ($eventForm->isSubmitted() && $eventForm->isValid()) {
            $events = $eventService->search($searchEvent)['records'];
           // dd($events);
        } else {
            $events = [];
        }

        return $this->render('event/index.html.twig', [
            'events' => $events,
            'eventForm' => $eventForm
        ]);
    }
}
