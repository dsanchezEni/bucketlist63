<?php

namespace App\Controller;

use App\Entity\Category;
use App\Models\Region;
use App\Repository\CategoryRepository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ApiCategoryController extends AbstractController
{
    #[Route('/api/v1/categories',name:'app_category_list',methods:['GET'])]
    public function list(CategoryRepository $categoryRepository, SerializerInterface $serializer):JsonResponse{
        //Requête vers la BD à travers le repository pour récupérer toutes les catégories présentes dans ma BD.
        $categories = $categoryRepository->findAll();
        //On sérialise les données au format JSON
        $resultat = $serializer->serialize($categories,'json',['groups'=>'getCategoriesFull']);
        //On retourne une réponse en JSON
        return new JsonResponse($resultat,Response::HTTP_OK,[],true);
        //Le 4ieme paramatre précise que je fournis déjà du JSON ici true !
    }
    #[Route('/api/v1/categories/{id}',name:'app_category_read',methods:['GET'])]
    public function read(int $id,?Category $category,CategoryRepository $categoryRepository, SerializerInterface $serializer):JsonResponse
    {
        $category=$categoryRepository->find($id);
        //test si la catégorie existe si elle n'existe pas retourne une reponse erreur 404 NOT_FOUND.
        if(!$category){
            return new JsonResponse(null,Response::HTTP_NOT_FOUND);
        }
        $resultat=$serializer->serialize($category,'json',['groups'=>'getCategories']);

        return new JsonResponse($resultat,Response::HTTP_OK,[],true);
    }
    #[Route('/api/v1/categories',name:'app_category_create',methods:['POST'])]
    public function create(Request $request, SerializerInterface $serializer,EntityManagerInterface $em):JsonResponse
    {
        //On récupére les données de la requete
        $data = $request->getContent();
        $category=$serializer->deserialize($data,Category::class,'json');
        $em->persist($category);
        $em->flush();

        //On sérialise le nouvel objet
        $categoryJson =$serializer->serialize($category,'json',['groups'=>'getCategories']);
        return new JsonResponse($categoryJson,Response::HTTP_CREATED,[
            "Location"=>$this->generateUrl('app_category_read',
                ["id"=>$category->getId()],
                UrlGeneratorInterface::ABSOLUTE_URL)
        ],
            true);
    }
}