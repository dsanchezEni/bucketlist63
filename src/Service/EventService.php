<?php

namespace App\Service;

use App\Models\SearchEvent;
class EventService
{
    public function search(SearchEvent $searchEvent) : array
    {
        $url =
            'https://public.opendatasoft.com/api/records/1.0/search/?dataset=evenements-publics-openagenda';
        $content = file_get_contents($url
            .'&refine.location_city='.ucfirst($searchEvent->city)
            .'&refine.firstdate_begin='.$searchEvent->dateEvent->format('Y-m-d'));
       // dd($content);
        return json_decode($content, true);
    }
}