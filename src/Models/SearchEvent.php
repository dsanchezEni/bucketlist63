<?php

namespace App\Models;
use Symfony\Component\Validator\Constraints as Assert;
class SearchEvent
{
    #[Assert\NotBlank()]
    public \DateTimeImmutable $dateEvent;

    #[Assert\NotBlank()]
    #[Assert\Length(max:255)]
    public string $city;

    /**
     * @return \DateTimeImmutable
     */
    public function getDateEvent(): \DateTimeImmutable
    {
        return $this->dateEvent;
    }

    /**
     * @param \DateTimeImmutable $dateEvent
     * @return SearchEvent
     */
    public function setDateEvent(\DateTimeImmutable $dateEvent): SearchEvent
    {
        $this->dateEvent = $dateEvent;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return SearchEvent
     */
    public function setCity(string $city): SearchEvent
    {
        $this->city = $city;
        return $this;
    }


}