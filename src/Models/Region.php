<?php

namespace App\Models;

class Region
{
    private ?string $code=null;
    private ?string $nom=null;

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return 'N° '.$this->code;
    }

    /**
     * @param string|null $code
     * @return Region
     */
    public function setCode(?string $code): Region
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param string|null $nom
     * @return Region
     */
    public function setNom(?string $nom): Region
    {
        $this->nom = $nom;
        return $this;
    }


}