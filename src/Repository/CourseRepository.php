<?php

namespace App\Repository;

use App\Entity\Course;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Course>
 *
 * @method Course|null find($id, $lockMode = null, $lockVersion = null)
 * @method Course|null findOneBy(array $criteria, array $orderBy = null)
 * @method Course[]    findAll()
 * @method Course[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Course::class);
    }

    public function save(Course $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Course $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Course[]
     */
    public function findLastCourses(int $duration = 2): Paginator {
        // en DQL
//        $entityManager = $this->getEntityManager();
//        $dql = "SELECT c
//                FROM App\Entity\Course c
//                WHERE c.duration > :duration
//                ORDER BY c.dateCreated DESC";
//        $query = $entityManager->createQuery($dql);
//        $query->setParameter('duration', $duration)
//            ->setMaxResults(5);
//        return $query->getResult();

        // version QueryBuilder
        $queryBuilder = $this->createQueryBuilder('c')
            ->addSelect('ca')
            ->addSelect('f')
            ->leftJoin('c.category', 'ca')
            ->leftJoin('c.trainers', 'f')
            ->andWhere('c.duration > :duration')
            ->addOrderBy('c.dateCreated','DESC')
            ->setParameter('duration', $duration)
            ->setMaxResults(5);
        $query = $queryBuilder->getQuery();
        //return $query->getResult();
        return new Paginator($query);
    }

//    /**
//     * @return Course[] Returns an array of Course objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Course
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
