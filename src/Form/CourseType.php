<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Course;
use App\Entity\Trainer;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class CourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
//        $builder
//            ->add('name')
//            ->add('content')
//            ->add('duration')
//            ->add('published')
//            ->add('dateCreated')
//            ->add('dateModified')
//        ;
        $builder
            ->add('name', TextType::class, [
                'label' => 'Titre'
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Description',
                'required' => false
            ])
            ->add('duration', IntegerType::class, [
                    'label' => 'Durée (jours)'
                ])
            ->add('filename', TextType::class, [
                'label' => 'Nom fichier',
                'required' => false])

            ->add('image', FileType::class, [
                'label' => 'Image',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new Image(['maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image',
                    ])
                ],
            ])
            ->add('published', CheckboxType::class, [
                'label' => 'Publié',
                'required' => false])

            ->add('category', EntityType::class, [
                'label' => 'Catégorie',
                'class' => Category::class,
                'choice_label' => 'name',
                'placeholder' => '--Choisir une catégorie--'
            ])
            ->add('trainers', EntityType::class, [
                'label' => 'Formateurs',
                'class' => Trainer::class,
                'choice_label' => 'fullname',
                'placeholder' => 'Choisir un formateur',
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('f')
                        ->orderBy('f.lastname', 'ASC')
                        ->addOrderBy('f.firstname', 'ASC');
                },
            ])
            ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
            $course = $event->getData();
            if ($course && $course->getFilename()) {
                // cas où on est en modification et qu'une image est déjà présente,
                // on ajoute un checkbox pour permettre de demander
                // la suppression de l'image
                $form = $event->getForm();
                $form->add('deleteImage', CheckboxType::class, [
                    'required' => false,
                    'mapped' => false,
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Course::class,
        ]);
    }
}
