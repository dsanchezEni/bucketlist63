<?php

namespace App\DataFixtures;

use App\Entity\Course;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
class CourseFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = \Faker\Factory::create('fr_FR');

        $course = new Course();
        $course->setName('Symfony');
        $course->setContent('Le développement web côté serveur (avec Symfony)');
        $course->setDuration(10);
        $course->setDateCreated(new \DateTimeImmutable('2023-01-01'));
        $course->setPublished(true);
        $course->setCategory($this->getReference('category1'));
        $this->addTrainers($course);
        $manager->persist($course);

        $course = new Course();
        $course->setName('PHP');
        $course->setContent('Le développement web côté serveur (avec PHP)');
        $course->setDuration(5);
        $course->setDateCreated(new \DateTimeImmutable('2022-01-01'));
        $course->setPublished(true);
        $course->setCategory($this->getReference('category1'));
        $this->addTrainers($course);
        $manager->persist($course);

        $course = new Course();
        $course->setName('Apache');
        $course->setContent('Administration d\'un serveur Apache sous Linux');
        $course->setDuration(5);
        $course->setDateCreated(new \DateTimeImmutable('2021-01-01'));
        $course->setPublished(true);
        $course->setCategory($this->getReference('category2'));
        $this->addTrainers($course);
        $manager->persist($course);

        // Création de 30 cours supplémentaires
        for ($i = 1; $i <= 30; $i++) {
            $course = new Course();
            $course->setName("Cours $i");
            $course->setContent("Description du cours $i");
            $course->setDuration(mt_rand(1,10));
            $course->setDateCreated(new \DateTimeImmutable());
            $course->setPublished(false);
            $course->setCategory($this->getReference('category1'));
            $this->addTrainers($course);
            $manager->persist($course);
        }

        // Création de 30 cours supplémentaires
        for ($i = 1; $i <= 30; $i++) {
            $course = new Course();
            $course->setName($faker->word());
            $course->setContent($faker->realText());
            $course->setDuration(mt_rand(1,10));
            $dateCreated = $faker->dateTimeBetween('-2 months', 'now');
            // NB : dateTimeBetween() retourne un DateTime, il faut donc le convertir en DateTimeImmutable
            $course->setDateCreated(\DateTimeImmutable::createFromMutable($dateCreated));
            $dateModified = $faker->dateTimeBetween($course->getDateCreated()->format('Y-m-d'), 'now');
            $course->setDateModified(\DateTimeImmutable::createFromMutable($dateModified));
            $course->setPublished(false);
            $course->setCategory($this->getReference('category'.mt_rand(1,2)));
            $this->addTrainers($course);
            $manager->persist($course);
        }

        $manager->flush();

    }

    public function getDependencies(): array
    {
        return [CategoryFixtures::class, TrainerFixtures::class];
    }

    private function addTrainers(Course $course): void {
        for ($i=0; $i <= mt_rand(0,5); $i++) {
            $trainer = $this->getReference('trainer'.rand(1,20));
            $course->addTrainer($trainer);
        }
    }
}
