<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{

    public function __construct(private readonly UserPasswordHasherInterface $userPasswordHasher)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = \Faker\Factory::create('fr_FR');
        //Création d'un administrateur
        $userAdmin = new User();
        $userAdmin->setEmail('admin@test.fr');
        $userAdmin->setLastname('admin');
        $userAdmin->setFirstname('admin');
        $userAdmin->setRoles(['ROLE_ADMIN']);
        //On hash le mot de passe
        $password = $this->userPasswordHasher->hashPassword($userAdmin,'123456');
        $userAdmin->setPassword($password);

        $manager->persist($userAdmin);
        //Création de 10 personnes classiques
        for($i=1;$i<=10;$i++){
            $user = new User();
            $user->setEmail("user$i@test.fr");
            $user->setLastname($faker->lastName());
            $user->setFirstname($faker->firstName());
            $user->setRoles(['ROLE_USER']);
            //On hash le mot de passe
            $password = $this->userPasswordHasher->hashPassword($user,'123456');
            $user->setPassword($password);
            $manager->persist($user);
        }

        $manager->flush();
    }
}
