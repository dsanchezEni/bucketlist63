<?php
namespace App\DataFixtures;
use App\Entity\Trainer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
class TrainerFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 1; $i <= 20; $i++) {
            $trainer = new Trainer();
            $trainer->setFirstname($faker->firstName());
            $trainer->setLastname($faker->lastName());
            $trainer->setDateCreated(new \DateTimeImmutable());
            $manager->persist($trainer);
            $this->addReference('trainer' . $i, $trainer);
        }
        $manager->flush();
    }
}